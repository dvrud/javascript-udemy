document.querySelector('#form').addEventListener('submit', function(e)
{

  // Hiding the calculate part.
 document.querySelector('#results').style.display = 'none';
  // Showing the loader
  document.querySelector('#loading').style.display = 'block';

  setTimeout(calculateLoan , 2000);
  e.preventDefault();
});

// Creating the function for calculating

function calculateLoan(e){
  // Getting the variables of form from UI.

  const UIamount = document.querySelector('#amount');
  const UIinterest = document.querySelector('#interest');

  const UIyears = document.querySelector('#years');
  const UImonthlypayment = document.querySelector('#monthly-payment');

  const UItotalpayment = document.querySelector('#total-payment');

  const UItotalinterest = document.querySelector('#total-interest');
// Starting the calculation part
  const principal = parseFloat(UIamount.value);
  const calculatedinterest = parseFloat(UIinterest.value)/100/12;
  const calculatedpayments = parseFloat(UIyears.value) * 12;
// Computing the monthly payment
const x = Math.pow(1 + calculatedinterest , calculatedpayments);
const monthly = (principal * x *calculatedinterest)/(x-1);

if(isFinite(monthly)){
UImonthlypayment.value = monthly.toFixed(2);
UItotalpayment.value = (monthly * calculatedpayments).toFixed(2);
UItotalinterest.value = ((monthly * calculatedpayments) - principal).toFixed(2);

 // showing the calculate part.
 document.querySelector('#results').style.display = 'block';
  // hiding the loader
  document.querySelector('#loading').style.display = 'none';

}
else{
showError('Please check your numbers');
}

}

// Creating a function for error

function showError(error)
{

 // hiding the calculate part.
 document.querySelector('#results').style.display = 'none';
  // hiding the loader
  document.querySelector('#loading').style.display = 'none';

  // Creating a div to show the error message.
  const errorDiv = document.createElement('div');
  
  // Getting the card elements
  const card = document.querySelector('.card');
  const heading = document.querySelector('.heading');

// Adding the bootstrap class to div.
errorDiv.className = 'alert alert-danger';

  // Inserting the error msg in div.
  errorDiv.appendChild(document.createTextNode(error));

  card.insertBefore(errorDiv , heading);

  // Clear error after 3 seconds
  setTimeout(clearError , 3000);


}

// Creating a function for clearing the error.
function clearError()
{
  document.querySelector('.alert').remove();
}