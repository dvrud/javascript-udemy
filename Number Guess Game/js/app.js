//Creating the variables for the game values.

let min = 1,
    max = 25,
    winningNum = getRandomNum(min, max),
    guessLeft = 3;

// Creating the variables for the UIEvent.

const form = document.querySelector('#form'),
      inputGuess = document.querySelector('.guess-input'),
      submitGuess = document.querySelector('.guess-btn'),
      maxNum = document.querySelector('.max-no'),
      minNum = document.querySelector('.min-no'),
      message = document.querySelector('.message');

      // Assigning the min and max values from here to the span content dynamically.
      maxNum.textContent = max;
      minNum.textContent = min;
      // creating the function for palying the game again.

      form.addEventListener('mousedown' , function(e){
       
        if(e.target.classList.contains('play-again'))
        {
          window.location.reload();
        }
      });

      // Creating the event listener for the game.

      form.addEventListener('submit' , function(e)
    {

      let guess = parseInt(inputGuess.value);

      //Validating the input

      if(isNaN(guess) || guess > max || guess < min)
      {
        setMessage(`Please enter the valid number between ${min} and ${max}.` , 'red')
      }
      // Checking if won.
      if(guess === winningNum)
      {
        // calling the function created below taking all the parameters below.
        gameOver(true , `${winningNum} is Correct , You Win.!`);
        // inputGuess.disabled = true;
        // setMessage(`${winningNum} is Correct , You Win.!` ,  'green');
        // inputGuess.style.borderColor = 'green';
      }
      else{
        guessLeft -= 1;
        
        if(guessLeft === 0)
        {
          // Game over - you lost.
           // calling the function created below taking all the parameters below.
        gameOver(false , `${winningNum} was Correct , You Lost!.`);
          // inputGuess.disabled = true;
          // setMessage(`${winningNum} was Correct , You Lost!.` ,  'red');
          // inputGuess.style.borderColor = 'red';
          
        }
        else{
          // Game continuous answer is wrong.

          inputGuess.style.borderColor = 'red';
          // clearing the input.

          inputGuess.value = '';

          // Reminding the user about the guesses.
          setMessage(`${guess} is not Correct.${guessLeft} guesses left.`,'red');

        }
      }
      e.preventDefault();
    });

    // Creating the function to give error when number input is not in the range.

    function setMessage(msg,color){
      message.textContent = msg;
      message.style.color = color;
    }
    function gameOver(won , msg)
    {
      let color;

      won === true ? color = 'green' : color = 'red';

      inputGuess.disabled = true;
      setMessage(msg , color);
      inputGuess.style.borderColor = color;
      message.style.color = color;

      submitGuess.value = 'Play-Again';
      submitGuess.className += ' play-again';


    }
    // Defining function for obtaining random number.
    function getRandomNum(min, max)
    {
     return Math.floor(Math.random()*(max-min+1)+min);
    }