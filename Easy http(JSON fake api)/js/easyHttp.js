function easyHttp(){
  this.http = new XMLHttpRequest();
}
// Get method to get the data
easyHttp.prototype.get = function(url , callback){

  this.http.open('GET' , url , true);
  
let self = this;

  this.http.onload = function(){
    if(self.http.status = 200)
    {
      callback(null , JSON.parse(self.http.responseText));
    }
    else{
      callback( 'ERROR:' + self.http.status , null);
      // If in future any error occurs in the above code just remove the null, cz null was the experiment subject.
    }
}
  this.http.send();
}

// Important....if u are using callback functions and u want to pass lesser values than the ones defined in the function then u can do it so because it is a callback function.

// U can send the posts here if in json.parse if u have embedded the posts back in the library js by json.stringify.

// Post method to post the data with the new id

easyHttp.prototype.post = function(url , data , callback){


this.http.open('POST' , url , true);
this.http.setRequestHeader('Content-type' , 'application/json');
let self = this;

this.http.onload = function(){
  callback(null , self.http.responseText);

}
this.http.send(JSON.stringify(data));

}

// Put method to update the data
easyHttp.prototype.put = function(url , data , callback){


  this.http.open('PUT' , url , true);
  this.http.setRequestHeader('Content-type' , 'application/json');
  let self = this;
  
  this.http.onload = function(){
    callback(null , self.http.responseText);
  
  }
  this.http.send(JSON.stringify(data));
  
  }

  // Delete method to delete the poost.
  easyHttp.prototype.delete = function(url , callback){

    this.http.open('DELETE' , url , true);
    
  let self = this;
  
    this.http.onload = function(){
      if(self.http.status = 200)
      {
        callback(null , 'Post deleted successfully.');
      }
      else{
        callback( 'ERROR:' + self.http.status , null);
       
      }
  }
    this.http.send();
  }