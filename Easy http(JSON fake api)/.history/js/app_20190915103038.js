//Inporting the easy http file in this file by making an object of it.
const http = new easyHttp();
// const request = require("request");
const n = {
  email: "dharmik@xcitech.in",
  password: "ds374332"
};
function call() {
  fetch("https://apiv2.shiprocket.in/v1/external/auth/login", {
    method: "GET",
    body: n
  })
    .then(datas => {
      console.log(datas, "this is the data");
    })
    .catch(err => {
      console.log(err, "this is the error");
    });
  // request.post(
  //   "https://apiv2.shiprocket.in/v1/external/auth/login",
  //   {
  //     form: {
  //       email: "dharmik@xcitech.in",
  //       password: "ds374332"
  //     }
  //   },
  //   (error, response, body) => {
  //     console.log(JSON.parse(body));
  //     res.send({ data: JSON.parse(body) });
  //     if (error) {
  //     }
  //   }
  // );
}
// Get posts
// Commenting the code below to test the new code out there.The code is fully functional .so dont discard.
// http.get('https://jsonplaceholder.typicode.com/posts', function(err , posts){
//   if(err)
//   {
//     console.log(err);
//   }
//   else{
//     console.log(posts);
//   }
// });

// U can get the posts here if in json.stringify if u have embedded the posts back in the library js by json.parse.
// To get a single post by the above method just use /1 after the url above and u will get the single post.

// Post the posts
// Creating an object containing the data that we want to post.
const data = {
  title: "Dharmik",
  body: "The best programmer of the centuary."
};
const data_updated = {
  title: "Dharmik",
  body: "The best programmer and designer of the centuary."
};

http.post("https://jsonplaceholder.typicode.com/posts", data, function(
  err,
  post
) {
  if (err) {
    console.log(err);
  } else {
    console.log(post);
  }
});

// Creating the put request.Updates the post
http.post("https://jsonplaceholder.typicode.com/posts", data_updated, function(
  err,
  post
) {
  if (err) {
    console.log(err);
  } else {
    console.log(post);
  }
});
// Deleting the post.
http.delete("https://jsonplaceholder.typicode.com/posts/1", function(
  err,
  response
) {
  if (err) {
    console.log(err);
  } else {
    console.log(response);
  }
});
