// First of all define all the variables needed to work on.
const form = document.querySelector('#addform');
const ul = document.querySelector('.collections');
const cleartask = document.querySelector('#cleartasks');
const filter = document.querySelector('#filter');
const  taskInput= document.querySelector('#newtasks');

// Initialize a function to Load all event listeners

loadEventListeners();

// Load all event listeners

function loadEventListeners()
{
  // DomContentload function for loading the content of local storage.
  getTasks();
  //Event for adding the li in the form.
  form.addEventListener('submit', addTask);
  //Event for removing the task.
  ul.addEventListener('click' , removeTask);
  // Event for removing all the tasks that is the clear button
  cleartask.addEventListener ('click', clearTasks);
  // Event for filtering the tasks
  filter.addEventListener('keyup', filterTasks);
}
// Creating the function for loading the task from local storage.
function getTasks(task)
{
  let tasks;
  if (localStorage.getItem('tasks') === null)
  {
    tasks = [];
  }
  else{
    tasks = JSON.parse(localStorage.getItem('tasks'));
  }

    tasks.forEach(function(task)
  {
    const li = document.createElement('li');

    li.appendChild(document.createTextNode(task));

    li.className = 'collection-item';

    const a = document.createElement('a');

    a.innerHTML = '<i class="fas fa-ban"></i>';
    a.className = 'delete-item ml-5';

    li.appendChild(a);

    ul.appendChild(li);
  });

}
// Creating the code for adding the task

function addTask(e)
{
    if(taskInput.value === '')
    {
      alert("Please Enter the Task");
    }
    else
    {

    
    const li = document.createElement('li');

    li.appendChild(document.createTextNode(taskInput.value));

    li.className = 'collection-item';

    const a = document.createElement('a');

    a.innerHTML = '<i class="fas fa-ban"></i>';
    a.className = 'delete-item ml-5';

    li.appendChild(a);

    ul.appendChild(li);
          // Function to store the tasks in the local storage through json

    storeInLocalStorage(taskInput.value);
    taskInput.value = '';
    }

     

  e.preventDefault();
}

// Function genration for storing the values in the local storage
function storeInLocalStorage(task)
{
  let tasks;
  if (localStorage.getItem('tasks') === null)
  {
    tasks = [];
  }
  else{
    tasks = JSON.parse(localStorage.getItem('tasks'));
  }

  tasks.push(task);
  localStorage.setItem('tasks', JSON.stringify(tasks));
}

// Function generation for removing a single task by clicking on the icon
function removeTask(e)
{
  if (e.target.parentElement.classList.contains('delete-item'))
  {
    if(confirm('Are You Sure?')){

    
      e.target.parentElement.parentElement.remove();

      removeFromLs(e.target.parentElement.parentElement);

    }
  }
}
// Function for removing the tasks from the database.
function removeFromLs(taskItem)
{
  let tasks;
  if (localStorage.getItem('tasks') === null)
  {
    tasks = [];
  }
  else{
    tasks = JSON.parse(localStorage.getItem('tasks'));
  }

  tasks.forEach(function(task, index)
  {
    if(taskItem.textContent === task)
    {
      tasks.splice(index, 1);
    }
    localStorage.setItem('tasks', JSON.stringify(tasks));
  });
}
// Function generation for removing all tasks by clciking on the clear task button.

function clearTasks(e)
{
  // A simple way
  ul.innerHTML = '';

  // A faster way through while loop

  while(ul.firstChild)
  {
    ul.removeChild(ul.firstChild);
  }
  // Function for clearing all the tasks stored in the local storage.
    clearTasksFromLs();
  // Proof that while loop is faster is given below.
  // https://jsperf.com/innerhtml-vs-removechild

}
// Function generation for clearing all the tasks from local storage.
function clearTasksFromLs()
{
  localStorage.clear();
}
// Function generation for filtering the tasks

function filterTasks(e)
{
  const text = e.target.value.toLowerCase();
  document.querySelectorAll('.collection-item').forEach(
    function(task)
    {
      const item = task.firstChild.textContent;

      if(item.toLowerCase().indexOf(text) != -1)
      {
          task.style.display =  'block';
      }
      else
      {
        task.style.display = 'none';
      }
    }
  );
}