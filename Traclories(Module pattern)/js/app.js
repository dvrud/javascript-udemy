// Storage COntroller

// Item Controller

const ItemController = (function() {
// Item Constructor
  const Item = function(id , name , calories) {
    this.id = id;
    this.calories = calories;
    this.name = name;

  }

  // Data structure

  const data = {
    items : [
      {id : 0 , name:'Burger' , calories : 900},
      {id : 1 , name:'Pizza' , calories : 800},
      {id : 2 , name:'Noodles' , calories : 700}
    ] , 
    currentItem : null,
    totalCalories : 0
  }


  return{
   
    getItems : function() {
      return data.items;
    },
    addItems : function(mealName , calories) {
      
      let id;

      if(data.items.length > 0){
        id = data.items[data.items.length -1].id + 1;
      }else {
        id = 0;
      }
      


      calories = parseInt(calories);

      newItem = new Item(id , mealName , calories);

      data.items.push(newItem);
      return newItem;
    },
    items : function() {
      return data;
    }
  }
})();

// UI Controller
const UiController = (function() {

  const UiSelector = {
    itemlist : '.item-lists',
    calorieInput : '#calorie-input',
    mealInput : '#meal-input',
    mealInputBtn : '#meal-input-btn'
  }

  //public methods
return{
  populateItems : function(items){
    let html = '';
    items.forEach(function(item) {
      html += `  <li class="list-group-item collection-item" id="item-${item.id}"> <strong>${item.name}</strong> : <em>${item.calories}</em>  <a href="" class="collection-item-link"><i class="edit-link fa fa-pencil"></i></a><li>`;
    });

    document.querySelector(UiSelector.itemlist).innerHTML = html;
  },
  getItemInput : function() {
    return{
      name : document.querySelector(UiSelector.mealInput).value,
      calories : document.querySelector(UiSelector.calorieInput).value
    }
  },
  getSelectors : function() {
    return UiSelector;
    
  }
}  
})();
// App Controller

const AppController = (function(ItemController , UiController) {
  
  const loadEventListener = function() {
    const Uicontrols = UiController.getSelectors();

    document.querySelector(Uicontrols.mealInputBtn).addEventListener('click' , itemAddSubmit);
  }

  const itemAddSubmit = function(e) {

    const input = UiController.getItemInput();
  
    if(input.name !== '' && input.calories !== '')
    {
      const addItem = ItemController.addItems(input.name , input.calories);
    }
    e.preventDefault();
  }
  return{
    init : function(){
      console.log('Initializing App.....');
      let items = ItemController.getItems();
      UiController.populateItems(items);


      loadEventListener();
    }

   
  }
})(ItemController , UiController);

AppController.init();