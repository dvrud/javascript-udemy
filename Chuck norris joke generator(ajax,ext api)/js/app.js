document.querySelector('#joke-button').addEventListener('click' , getJokes);

function getJokes(e){
  const joke_numbers = document.querySelector('#joke-number').value;

  const xhr = new XMLHttpRequest();

  xhr.open('GET' , `http://api.icndb.com/jokes/random/${joke_numbers}` , true);

  xhr.onload = function(){


  if(this.status === 200){
    const response = JSON.parse(this.responseText);
    let output = '';

    if(response.type === 'success')
    {
      response.value.forEach(function(joke){

        output += `<li>${joke.joke}</li>`
      });


    }
    else
    {
      output += `<li>Something went wrong.</li>`
    }

    document.querySelector('#joke-display').innerHTML = output;

  }
 
  }
  
  xhr.send();
  e.preventDefault();
}