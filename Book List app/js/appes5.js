// Creating the constructor for Book-Form
  function Book(title ,author , isdn)
  {
    this.title = title;
    this.author = author;
    this.isdn = isdn;
  }
// Creating the constructor for  UI.
function UI()
{

}

// Creating the prototype method for the UI constructor.

UI.prototype.addFields = function(book)
{
  // Getting the t-body from the table.
const list = document.querySelector('#book-list');
// Creating the tr element to append in the table.
const row = document.createElement('tr');
// Inserting the column in the tr element through innerHtml.
row.innerHTML = `<td>${book.title}</td>
<td>${book.author}</td>
<td>${book.isdn}</td>
<td><a class="delete">X</a></td>


`;

list.appendChild(row);
}

// Creating the UI prototype method for generating the error.
 UI.prototype.getAlert = function(message , className){
  //  Creating the element
   const h5 = document.createElement('h5');
  // Getting the parent element to append the child.
   const card = document.querySelector('.card-header');

   h5.className = `${className}` ;
   h5.appendChild(document.createTextNode(message));
  const before = document.querySelector('.card-body');

  card.appendChild(h5);

  setTimeout(function(){
    document.querySelector(`.${className}`).remove();
  },3000);
 }

//  Creating the UI prototype function to remove the book.


UI.prototype.removeBook = function(target){


  if(target.className === 'delete')
  {

  target.parentElement.parentElement.remove();
  }
}

// Creating the prototype method for the UI constructor.

UI.prototype.clearFields = function(){
  document.querySelector('#book-name').value = '';
   document.querySelector('#author-name').value = '';
  document.querySelector('#isdn-no').value = '';
}
// Creating the event for the form when submitted.

document.querySelector('#book-form').addEventListener('submit' , function(e){

const title = document.querySelector('#book-name').value,
      author = document.querySelector('#author-name').value,
      isdn = document.querySelector('#isdn-no').value;
// Creating the book object.
const book = new Book(title , author , isdn);
// Creating the UI object.

const ui = new UI();
//Validating the form through if else condition.
if(title === '' || author === '' || isdn === ''){
  ui.getAlert('Please fill in all the fields' , 'bg-danger')
}
else{
// Creating the UI objects prototype which is defined above.

ui.addFields(book);

// Showing alert when the book is added.
ui.getAlert('Book Added' , 'bg-success')
// Clearing the fields in the form after adding it to the table.
ui.clearFields();
}

e.preventDefault();
});

// Creating the event delegation for removing the book.
// Remember when anything is dynamically added u should use event delegation
// and event delegation means  to take the event listener to the parent instead of direct listening.

document.querySelector('#book-list').addEventListener('click' , function(e){

// instantiating the UI object to use here.
const ui = new UI();

// Creating a prototype function to delete the book.

ui.removeBook(e.target);

// Showing the message for removing the book.
ui.getAlert('Book Removed!' , 'bg-warning');
  e.preventDefault();
});