// INitializing the github class from the githubprofile file

const githubprofile = new Github;
// Init UI class to show the profile.

const ui = new UI;

// Searching profiles
const searchKeyword = document.querySelector('#profile-search');

// adding event listener to the keyword searched

searchKeyword.addEventListener('keyup' , (e) => {

  const searchKeywordText = e.target.value;

  if(searchKeywordText !== '')
  {
  githubprofile.getUser(searchKeywordText)
  .then(res => {
  if(res.profile.message === 'Not Found')
  {
   
    // Show an alert which will be in ui.js
    ui.showProfile(res.profile,'no-profile');
    ui.showRepos(res.repos , 'no-profile');
    
  }
  else{
    console.log(res );
    // show the profile which will also be shown in ui.js
    ui.showProfile(res.profile ,'profile');
    ui.showRepos(res.repos , 'profile');

  }

})
 
  }
  else{
    // clear the profile as the word written is being erased.
    ui.clearErrorSub();

  }


})