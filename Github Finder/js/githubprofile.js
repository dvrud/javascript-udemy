class Github{
  constructor() {
    this.client_id = '8e4952c856765f6498a0';
    this.client_secret = '58c6960b535d9d898b3484155a34ad56d16db2b2';
    this.repos_count = 5;
    this.repos_sort = 'asc';
  }

  async getUser(user){
    const profile = await fetch(`https://api.github.com/users/${user}?client_id=${this.client_id}&client_secret=${this.client_secret}`);

    const repos = await fetch(`https://api.github.com/users/${user}/repos?per_page=${this.repos_count}&sort=${this.repos_sort}client_id=${this.client_id}&client_secret=${this.client_secret}`);


    const profileData = await profile.json();
    const reposData = await repos.json();

    return{
      profile : profileData,
      repos : reposData
    }
  }
}