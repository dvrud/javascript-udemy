class UI{
  constructor() {
    this.profile_output = document.querySelector('#profile-output');
    this.error_output = document.querySelector('#error-output');
  }

  showProfile(user , adv){
    if(adv === 'profile')
    {

    // let output = '';
    // const repos = user.repos_url;
    // repos.forEach((posts) => {
    //   output += `<li>${posts}</li>`
    // })
    this.profile_output.innerHTML = `
    <div class="container toGet-1">
    <div class="row">
            <div class="col-md-3">
                    <img src="${user.avatar_url}" alt="" class="img-fluid mb-3">
                    <a href="${user.html_url}" target=_blank class="btn btn-outline-primary mb-3">Get to Profile</a>
            </div>
            <div class="col-md-9">
                    <span class="badge bg-primary">Gists Url:${user.gists_url}</span>
                    <span class="badge bg-success">User id:${user.id}</span>

                    <span class="badge bg-info">User Name:${user.name}</span>

                    <span class="badge bg-dark">User Node Id:${user.node_id}</span>

                    <ul class="list-group">
                            <li class="list-group-item">public Gists:${user.public_gists}</li>
                            <li class="list-group-item">public Repos:${user.repos}</li>
                            <li class="list-group-item">Followers:${user.followers}</li>
                            <li class="list-group-item">Following:${user.following}</li>
                    </ul>

            </div>
    </div>
     
</div>
    `;
    }
    else if(adv === 'no-profile')
    {
      this.profile_output.innerHTML = `
      <div class="container toGet">
      <div class="row">
              <div class="col bg-dark text-white">
                No profile Found
              </div>
      </div>
  </div>
      `;

      setTimeout(this.clearError , 3000);
    }
  }
  showRepos(repos , adv)
  {
    
    if(adv === 'profile')
    {
    let output = '';
    repos.forEach((repo) => {
      output += `
      <li><a href="${repo.html_url}" target=_blank>${repo.name}</a></li>
      `;
      this.error_output.innerHTML = output;
    })
  }
  else if(adv === 'no-profile'){

  }
  }
  // Method to show the error
  showError(msg){

    this.error_output.innerHTML = ` 
     <div class="container">
    <div class="row">
            <div class="col bg-alert text-white">
              No profile Found
            </div>
    </div>
</div>`; 

    setTimeout(this.clearError() , 3000);
  }

  // method to clear the error
  clearError(){

    const check = document.querySelector('.toGet');

    if(check){
      check.remove();
    }
  }

  clearErrorSub(){
    const check = document.querySelector('.toGet-1');

    check.innerHTML = '';
    console.log('works');
  }
}